
import { Route,Routes } from 'react-router-dom'
import UserLayout from "./layout/UserLayout"
import Detail from './page/Detail/Detail'

function App() {
  

  return (
    <>
    <Routes>
  <Route path="/" element={<UserLayout/>}>
<Route path='/movie/:id' element={<Detail/>}></Route>
</Route>
    </Routes>
    
    </>
  )
}

export default App
