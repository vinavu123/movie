import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { TOKEN } from '../../contants/contans';

export default function Detail() {
  // lấy id từ url
  const { id } = useParams();
  // {id : 11771}

  const [movie, setMovie] = useState({});

  // call api lấy chi tiết phim
  useEffect(() => {
    if (id) {
      axios
        .get(
          `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
          {
            headers: {
              TokenCybersoft: TOKEN,
            },
          }
        )
        .then((res) => {
          console.log("result", res);
          setMovie(res.data.content);
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  }, [id]);
  console.log("movie", movie);

  
  return (
    <div className='bg-black'>
<div class="card ">
 
  <div class="card-body bg-dark text-light">
  <h1><h1><img src={movie.hinhAnh} className='card-header'/></h1>
  <br />
  <h2>Trailer</h2>
   <div class="embed-responsive embed-responsive-21by9">
  <iframe class="embed-responsive-item" src={movie.trailer}></iframe>
</div></h1>
    <h5 class="card-title">{movie.tenPhim}</h5>
    <p class="card-text">{movie.moTa}</p>
    <h3>Ngày chiếu: {movie.ngayKhoiChieu}</h3>
   
    <a href="#" class="btn btn-primary">Book</a>
    

 
  </div>
</div>
    </div>
  )
}
