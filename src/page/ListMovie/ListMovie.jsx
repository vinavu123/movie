import React, { useState, useEffect} from 'react'
import axios from "axios"
import { useNavigate } from 'react-router-dom'
import {TOKEN }from "../../contants/contans"
export default function ListMovie() {
  const [listMovie,setListMovie] = useState([])
  const navigate = useNavigate()
  useEffect(()=>{
axios.get(
"https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01",
{
  headers:{
    TokenCybersoft: TOKEN
  }
}

).then((res)=>{
  setListMovie(res.data.content)
  })
  .catch((err)=>{console.log(err);})

  },[])
  
  return (
    <div className="container">
      <h1>List Film</h1>
      <div className="row">
        {listMovie.map((item) => {
          return (
            <div className="col-3" key={item.maPhim}>
              <div className="card bg-dark text-light m-1">
                <img
                  src={item.hinhAnh}
                  width="100%"
                  height={300}
                  style={{ objectFit: "cover" }}
                />
                <div className="card-body">
                  <p>Tên Phim:{item.tenPhim}</p>
                  
                  <p>Ngày chiếu:{item.ngayKhoiChieu}</p>

                  <button
                    onClick={() => {
                      navigate(`/movie/${item.maPhim}`);
                    }}
                    className="btn "
                  >
                    Detail
                  </button>
                  
                  <button
                    className="btn btn-success ml-5"
                  >
                    Book
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  )
}
