import axios from 'axios'
import React, {useEffect, useState}from 'react'
import { TOKEN } from '../../../contants/contans'
TOKEN
export default function Baner() {
    const [baner,setBaner] = useState({})
    useEffect(()=>{
        axios.get(
"https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachBanner",
{headers:{
    TokenCybersoft: TOKEN,
}}

        )
        .then((res)=>{
        setBaner(res.data.content[0])
        console.log(res.data);
        })
        .catch((err)=>{console.log(err);})


    },[])
    
  return (
    <div >
     <img src={baner.hinhAnh} />

    </div>
  )
}
