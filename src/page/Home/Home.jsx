
import React,{useState,useEffect} from 'react'
import ListMovie from '../ListMovie/ListMovie'
import Baner from './Baner/Baner'

export default function Home() {

   
  return (
    
    <div>
      <Baner></Baner>
      <ListMovie></ListMovie>
    </div>
  )
}
