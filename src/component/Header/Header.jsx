import React, { useState } from 'react'
import { NavLink,useNavigate } from 'react-router-dom'

export default function Header() {
  const navigate = useNavigate()

  return (
    
   <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
  <h2><NavLink className="nav-link" to={"/"}>Movie-Soft</NavLink></h2>
  
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <NavLink className="nav-link" to={"/"}>Home</NavLink>
      </li>
      
      
    </ul>
    
    <a><NavLink  >Login</NavLink> <NavLink >Register</NavLink></a>
    
  </div>
</nav>



  )
}
