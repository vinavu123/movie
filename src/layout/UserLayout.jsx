import React from 'react'
import Header from "../component/Header/Header"
import Footer from "../component/Footer/Footer"

import { Outlet } from 'react-router-dom'
import Body from '../component/Body/Body'
export default function UserLayout() {
  return (
    <div>
    <Header/>
    <Body/>
    <Outlet/>
    <Footer/>

    </div>
  )
}
